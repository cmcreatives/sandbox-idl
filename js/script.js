$ = jQuery.noConflict();

$(window).on("load", function() {

  "use strict";

  /* ===================================
          Loading Timeout
   ====================================== */

  setTimeout(function() {
    $(".loader").fadeOut("slow");
  }, 1000);

});

jQuery(function($) {

  "use strict";

  /* ===================================
          Scroll
  ====================================== */

  $(window).on('scroll', function() {
    if ($(this).scrollTop() >
      220) { // Set position from top to add class
      $('header').addClass('header-appear');
    } else {
      $('header').removeClass('header-appear');
    }
  });

  $(".progress-bar").each(function() {
    $(this).appear(function() {
      $(this).animate({
        width: $(this).attr("aria-valuenow") + "%"
      }, 3000)
    });
  });

  $('.count').each(function() {
    $(this).appear(function() {
      $(this).prop('Counter', 0).animate({
        Counter: $(this).text()
      }, {
        duration: 3000,
        easing: 'swing',
        step: function(now) {
          $(this).text(Math.ceil(now));
        }
      });
    });
  });

  //scroll to appear
  $(window).on('scroll', function() {
    if ($(this).scrollTop() > 500)
      $('.scroll-top-arrow').fadeIn('slow');
    else
      $('.scroll-top-arrow').fadeOut('slow');
  });

  // fixing bottom nav to top on scrolliing
  var $fixednav = $(".bottom-nav");
  $(window).on("scroll", function() {
    var $heightcalc = $(window).height() - $fixednav.height();
    if ($(this).scrollTop() > $heightcalc) {
      $fixednav.addClass("navbar-bottom-top");
    } else {
      $fixednav.removeClass("navbar-bottom-top");
    }
  });

  //Click event to scroll to top
  $(document).on('click', '.scroll-top-arrow', function() {
    $('html, body').animate({
      scrollTop: 0
    }, 800);
    return false;
  });

  //scroll sections
  if ($("body").hasClass("intrective")) {
    $(".scroll").on("click", function(event) {
      event.preventDefault();
      $("html,body").animate({
        scrollTop: $(this.hash).offset().top
      }, 1200);
    });

  } else {

    $(".scroll").on("click", function(event) {
      event.preventDefault();
      $("html,body").animate({
        scrollTop: $(this.hash).offset().top - 60
      }, 1200);
    });

  }

  /* ===================================
       Side Menu
   ====================================== */
  if ($("#sidemenu_toggle").length) {
    $("#sidemenu_toggle").on("click", function() {
      $(".pushwrap").toggleClass("active");
      $(".side-menu").addClass("side-menu-active"), $(
        "#close_side_menu").fadeIn(700)
    }), $("#close_side_menu").on("click", function() {
      $(".side-menu").removeClass("side-menu-active"), $(this)
        .fadeOut(200), $(".pushwrap").removeClass("active")
    }), $(".side-nav .navbar-nav .nav-link").on("click",
      function() {
        $(".side-menu").removeClass("side-menu-active"), $(
            "#close_side_menu").fadeOut(200), $(".pushwrap")
          .removeClass("active")
      }), $("#btn_sideNavClose").on("click", function() {
      $(".side-menu").removeClass("side-menu-active"), $(
          "#close_side_menu").fadeOut(200), $(".pushwrap")
        .removeClass("active")
    });
  }

  if ($(".side-right-btn").length) {

    $(".side-right-btn").click(function() {
        $(".navbar.navbar-right").toggleClass('show');
      }),
      $(".navbar.navbar-right .navbar-nav .nav-link").click(
        function() {
          $(".navbar.navbar-right").toggleClass('show');
        });
  }

  /* =====================================
        Wow
   ======================================== */

  if ($(window).width() > 767) {
    var wow = new WOW({
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 0,
      mobile: false,
      live: true,
    });
    new WOW().init();
  }

  /* ===================================
          Contact Form
   ====================================== */
  //Contact Us

  $("#contactForm").validate({
    rules: {
      first_name: {
        required: true,
      },
      last_name: {
        required: true,
      },
      last_name: {
        required: true,
      },
      email: {
        required: true,
      },
      message: {
        required: true,
      },

    },
    submitHandler: function(form) {
      var action = $(this).attr("action");
      $(form).ajaxSubmit({
        type: "POST",
        url: action,
        crossDomain: true,
        data: new FormData(this),
        dataType: "json",
        contentType: "multipart/form-data",
        processData: false,
        contentType: false,
        headers: {
          "Accept": "application/json"
        },
        success: function() {
          $('.success').addClass('is-active');
        },

      });
    }
  });

  /* ===================================
          Media Query
   ====================================== */

  var $el, $prt, $fade, $ps, $up, totalHeight;
  var $orgHeight = $('.cm-fade-text').height();
  var mqMaxMedium = window.matchMedia('(max-width: 769px)');
  var mqMinLarge = window.matchMedia('(min-width: 1024px)');
  var mqMaxXLarge = window.matchMedia('(max-width: 1281px)');
  if (mqMinLarge.matches) {
    //  READ MORE for PEDIGREE
    $("#read_more_pedigree").click(function() {
      totalHeight = 500
      $el = $(this);
      $prt = $el.parent();
      $up = $prt.prev();
      $fade = $up.find(".cm-fade-text");

      $up
        .css({
          // Set height to prevent instant jumpdown when max height is removed
          "height": $up.height(),
          "max-height": 9999,
        })
        .animate({
          "height": totalHeight
        })
      $fade
        .removeClass('cm-fade-text')
        .addClass('js-fade');

      $el.hide();
      $('#read_less_pedigree').fadeIn();
      // prevent jump-down
      return false;
    });

    $("#read_less_pedigree").click(function() {
      $el = $(this);
      $prt = $el.parent();
      $up = $prt.prev();
      totalHeight = 500;
      $fade = $up.find(".js-fade");
      $up
        .css({
          // Set height to prevent instant jumpdown when max height is removed
          "height": totalHeight,
          "max-height": 9999
        })
        .animate({
          "height": $orgHeight
        })
      $fade
        .addClass('cm-fade-text')
        .removeClass('js-fade');
      $el.hide();
      $('#read_more_pedigree')
        .fadeIn();
      if (mqMaxXLarge.matches) {
        $('#read_more_pedigree')
          .css({
            "margin-top": 100,
          });
      }
      return false;
    });

    //  READ MORE for WHY LABUAN
    $("#read_more_labuan").click(function() {
      totalHeight = 500
      $el = $(this);
      $prt = $el.parent();
      $up = $prt.siblings();
      $ps = $up.find("p");

      $up
        .css({
          // Set height to prevent instant jumpdown when max height is removed
          "height": $up.height(),
          "max-height": 9999,
        })
        .animate({
          "height": totalHeight
        })
        .removeClass('cm-fade-text-white');
      $el.hide();
      $('#read_less_labuan').fadeIn();
      // prevent jump-down
      return false;
    });

    $("#read_less_labuan").click(function() {
      $el = $(this);
      $prt = $el.parent();
      $up = $prt.siblings();
      totalHeight = $up.height();
      $up
        .css({
          // Set height to prevent instant jumpdown when max height is removed
          "height": totalHeight,
          "max-height": 9999
        })
        .animate({
          "height": $orgHeight
        })
        .addClass('cm-fade-text-white');
      $el.hide();
      $('#read_more_labuan').fadeIn();
      return false;
    });
  }

  if (mqMaxMedium.matches) {
    $("#read_more_pedigree").click(function() {
      $('#modal_pedigree').modal('toggle')
    });

    $("#read_more_labuan").click(function() {
      $('#modal_labuan').modal('toggle')
    });
  }

  /* ===================================
          Privacy Policy
   ====================================== */
   $("#read_privacy_policy, #link_privacy_policy, #nav_privacy_policy").click(function() {
    $('#modal_privacy').modal('toggle')
  });

  $("#read_cookie_policy, #banner_cookie_policy, #link_cookie_policy, #nav_cookie_policy").click(function() {
    $('#modal_cookie').modal('toggle')
  });

  $("#dismiss_banner").click(function() {
    $("#banner_cookie").hide()
  });

});