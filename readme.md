# Readme

## Contact Form
1. Used [Getform](https://getform.io/)  for the form backend - reasonable features for free tier, see [pricing](https://getform.io/pricing).
2. Alternatives include:
- [FormKeep](https://formkeep.com/)
- [Basin](https://usebasin.com/pricing)
- [FormBackend](https://www.formbackend.com/#pricing)
3. I chose GetForm for ease of integration, and also it allows 100 submissions a month for the free tier. It's pretty reasonable.
4. Sign up at the selected website.
5. In the `index.html` file, simply look for `#contactForm`.
6. Replace the `action` with the endpoint URL provided by the service you chose.
7. That's it!

### Note
- Required fields: First and last names, email address, and message. 
- Optional: phone number

## Social-media links
1. Search for `<!--NOTE: social media links-->`
2. Remove the comment tags accordingly.
3. Replace the `href` in the hyperlink tags with the social media profile.

### Note
- All social media icons are using [Font Awesome Icons v4.7.0](https://fontawesome.com/v4.7.0/icons/#brand). 
- If you want to add/replace the social media icon, just change the icon name in the `class` attribute. E.g. `i class="fa fa-facebook"`

